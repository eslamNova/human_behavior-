import csv 
import pandas as pd
import numpy as np
from sklearn import svm
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder

#reading data 
df = pd.read_csv('Final_Output.csv') #read data
#editing on data
df = df.fillna(0) #conver NaN values to zeros
df = df.drop('ID', 1) #drop ID column
df = df.drop('Frame Number', 1) #drop Frame Number column
array = df.values #conver df to array
X = np.array(array)
target = array[:, X.shape[1]-1:] #split target data

#Label Encoder
labelencoder = LabelEncoder()
target = labelencoder.fit_transform(target)

X = np.delete(X, X.shape[1]-1, 1) #drop target data 
Y = np.array(target)
Y=Y.astype('int') # issue

#feature scaling 
scaler = StandardScaler()
X = scaler.fit_transform(X)

#pca
pca = PCA(n_components=90)
pca.fit(X)
X = pca.transform(X)

#train test split 
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33, random_state=42)

#svm model
clf = svm.SVC()

clf.fit(X_train, y_train)

predicted = clf.predict(X_test)

#print accuracy
print("accuracy: ",accuracy_score(y_test, predicted))